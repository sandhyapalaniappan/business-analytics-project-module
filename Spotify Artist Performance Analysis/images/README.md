**Spotify Artists Analysis:**

The objective of this business analytics project module is to examine the musical diversity and similarities among various artists featured on Spotify. It aims to unravel the musical preferences of 50 distinct artists across numerous genres. During this study, I will also categorize artists into clusters based on shared musical styles.

For this analysis, I will utilize the Spotify Web API, which offers access to Spotify's extensive music catalog via HTTPS requests to an API endpoint. The API provides detailed track information, including audio features like danceability, instrumentalness, and tempo for each song. My research will concentrate on extracting these audio features from 50 different 'This Is' Playlists corresponding to 50 unique artists. Each audio feature captures a specific characteristic of a song. More information about how these features are computed is available on the [Spotify API Website](https://developer.spotify.com/documentation/web-api/).

Additionally, I have documented the findings of this study in a Medium blog post, which can be accessed [here](https://medium.com/@yourusername/spotify-this-is-playlists-the-ultimate-song-analysis-for-50-mainstream-artists): "Spotify's 'This Is' Playlists: The Ultimate Song Analysis for 50 Mainstream Artists".

## Required Software:

* Ensure you have the latest version of [RStudio](https://www.rstudio.com/).

## R Packages:

Below is a list of the R packages utilized in this project:

* [Rspotify](https://github.com/tiagomendesdantas/Rspotify) - To interact with the Spotify Web API.
* [httr](https://cran.r-project.org/web/packages/httr/index.html) - For handling HTTP requests.
* [jsonlite](https://cran.r-project.org/web/packages/jsonlite/index.html) - A robust and quick JSON parser.
* [readr](https://cran.r-project.org/web/packages/readr/index.html) - For reading rectangular data.
* [knitr](https://cran.r-project.org/web/packages/knitr/index.html) - For dynamic report generation in R.
* [kableExtra](https://cran.r-project.org/web/packages/kableExtra/index.html) - Enhancements for 'knitr::kable()' tables.
* [dplyr](https://cran.r-project.org/web/packages/dplyr/index.html) - A grammar of data manipulation.
* [gdata](https://cran.r-project.org/web/packages/gdata/index.html) - Various R programming tools for data manipulation.
* [radarchart](https://cran.r-project.org/web/packages/radarchart/index.html) - To create radar charts.
* [tidyr](https://cran.r-project.org/web/packages/tidyr/index.html) - For tidying data.
* [ggfortify](https://cran.r-project.org/web/packages/ggfortify/index.html) - To allow 'ggplot2' to handle some popular R packages.
* [ggthemes](https://cran.r-project.org/web/packages/ggthemes/index.html) - Additional themes for 'ggplot2'.
* [stringr](https://cran.r-project.org/web/packages/stringr/index.html) - Simple, consistent wrappers for common string operations.
* [ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html) - A system for 'declaratively' creating graphics.