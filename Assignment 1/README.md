This repository contains all resources for the Business Analytics Project module, which utilises R, Tableau, and Tableau Prep for comprehensive data analysis and visualization.

Thank you so much for your kind support, everyone!